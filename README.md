# Emacs Config

This repository contains the Emacs config I use for Rust and Python development.

Disabling TLS 1.3 is only used on Debian systems. Everywhere else has a recent
enough version to keep it enabled. When QubesOS moves the Debian templates from
Buster to Bullseye that line will disappear.
