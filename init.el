(set-face-attribute 'default t :font "Monospace-14")
(set-frame-font "Monospace-14" nil t)

(setq gnutls-algorithm-priority "NORMAL: -VERS-TLS1.3")

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)

(if (not (package-installed-p 'use-package))
  (progn
    (package-refresh-contents)
    (package-install 'use-package)))

(eval-when-compile
  (require 'use-package))

(use-package exec-path-from-shell
  :ensure t
  :config
  (when (memq window-system '(ns x))
    (exec-path-from-shell-initialize)))

(use-package evil
  :ensure t
  :config
    (evil-mode 1))

(use-package evil-org
  :ensure t
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package magit
  :ensure t)

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package lsp-mode
  :ensure t
  :bind-keymap
  ("C-M-l" . lsp-command-map))

(use-package lsp-jedi
  :ensure t
  :config
  (with-eval-after-load "lsp-mode"
    (add-to-list 'lsp-disabled-clients 'pyls)
    (add-to-list 'lsp-enabled-clients 'jedi))
  (add-hook 'python-mode-hook
	    (lambda ()
	      (lsp))))

(use-package company
  :ensure t)

(use-package yasnippet
  :ensure t)

(use-package yasnippet-snippets
  :ensure t)

(use-package rustic
  :ensure t
  :config
  (with-eval-after-load "lsp-mode"
    (add-to-list 'lsp-disabled-clients 'rls)
    (add-to-list 'lsp-enabled-clients 'rust-analyzer)))

(use-package restclient
  :ensure t)

(use-package company-restclient
  :ensure t)

(use-package base16-theme
  :ensure t
  :config
  (load-theme 'base16-irblack t))

(use-package powerline
  :ensure t)

(use-package airline-themes
  :ensure t)

(load-theme 'airline-minimalist t)

(setq evil-default-cursor (quote (t "#fc4e74"))
	evil-visual-state-cursor '("#04d623" box)
	evil-normal-state-cursor '("#fc4e74" box)
	evil-insert-state-cursor '("#d0d9f2" box))

(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-by-copying t)
 '(backup-directory-alist (quote (("." . "~/.emacs.d/backup"))))
 '(company-idle-delay 0.25)
 '(company-minimum-prefix-length 2)
 '(delete-old-versions t)
 '(display-line-numbers t)
 '(evil-default-cursor t)
 '(inhibit-startup-screen t)
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(kept-new-versions 5)
 '(package-selected-packages (quote (flycheck rustic lsp-mode magit use-package)))
 '(version-control t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
